function tellTime(hour, minute, period) {
    var almost = hour + 1;
    if (period == "AM" && minute > 30) {
        console.log("it's almost " + almost + " in the morning");
    }
    else if (period == "AM" && minute < 30) {
        console.log("it's just after " + hour + " in the morning");
    }
    else if (period == "AM" && minute == 30) {
        console.log("it's half past " + hour + " in the morning");
    }
    else if (period == "PM" && minute > 30) {
        console.log("it's almost " + almost + " in the evening");
    }
    else if (period == "PM" && minute < 30) {
        console.log("it's just after " + hour + " in the evening");
    }
    else if (period == "PM" && minute == 30) {
        console.log("it's half past " + hour + " in the evening");
    }
}
tellTime(8, 50, "AM")
tellTime(7, 15, "PM")