var number1 = 4;
var number2 = 100;
var number3 = -3;
var number4 = 4.2;
var string1 = "Coding";
var string2 = "Dojo";
var boolean1 = true;
var boolean2 = false;
var undeclared1

console.log("The First Number:", number1)
console.log("The Second Number:", number2)
console.log("The Third Number:", number3)
console.log("The Fourth Number:", number4)
console.log("The First String:", string1)
console.log("The Second String:", string2)
console.log("The First Boolean:", boolean1)
console.log("The Second Boolean:", boolean2)
console.log("The First Undeclared Variable:", undeclared1)