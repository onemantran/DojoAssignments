from flask import Flask, render_template, request, redirect, flash
app = Flask(__name__)
app.secret_key = 'rawr'

@app.route('/')
def index():
  return render_template("index.html")

@app.route('/results', methods=['POST'])
def sendresults():
  if len(request.form['name']) < 1 or len(request.form['comment']) < 1:
    flash("Name or comment cannot be empty!")
    return redirect('/')
  elif len(request.form['comment']) > 120:
    flash("Comment length too long!")
    return redirect('/')
  else:
    return render_template('results.html', name = request.form['name'], dojo = request.form['dojo'], language = request.form['language'], comment = request.form['comment'])

app.run(debug=True) 