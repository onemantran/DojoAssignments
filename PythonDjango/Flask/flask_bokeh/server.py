from flask import Flask, render_template, request, redirect, session
from bokeh.plotting import figure, output_file, show

from bokeh.resources import CDN
from bokeh.embed import file_html

app = Flask(__name__)
app.secret_key = 'rawr'

@app.route('/')
def index():
    session.clear()
    return render_template("index.html")

@app.route("/", methods=["post"])
def gather_data(): 
    session['x_axis'] = (request.form['x_axis']).split(',')
    session['y_axis'] = (request.form['y_axis']).split(',')
    return redirect("/success")

@app.route('/success')
def plotting():
    x = session['x_axis']
    y = session['y_axis']
    plot = figure(title="Your Graph", x_axis_label='x', y_axis_label='y')
    plot.line(x, y, legend="Temp.", line_width=2)
    return file_html(plot, CDN, "Flask Bokeh")
    
app.run(debug=True)

