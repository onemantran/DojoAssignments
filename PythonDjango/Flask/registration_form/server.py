from flask import Flask, render_template, request, redirect, flash
import re
from datetime import datetime

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
PASS_REGEX = re.compile(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{9,}$')
app = Flask(__name__)
app.secret_key = 'rawr'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def send_results():
    if (request.form['birth_date']) == '' or len(request.form['first_name']) < 1 or len(request.form['last_name']) < 1 or len(request.form['email']) < 1 or len(request.form['password']) < 1 or len(request.form['confirm_password']) < 1:
        flash('All fields are required and must not be blank')
        return redirect('/')
    elif datetime.strptime(request.form['birth_date'], '%Y-%m-%d') > datetime.now():
        flash('Birth-date must be from the past')
        return redirect('/')
    elif not (request.form['first_name']).isalpha():
        flash('First and Last Name cannot contain any numbers')
        return redirect('/')
    elif not (request.form['last_name']).isalpha():
        flash('First and Last Name cannot contain any numbers')
        return redirect('/')
    elif len(request.form['password']) < 9:
        flash('Password should be more than 8 characters')
        return redirect('/')
    elif not EMAIL_REGEX.match(request.form['email']):
        flash('Email should be a valid email')
        return redirect('/')
    elif not PASS_REGEX.match(request.form['password']):
        flash('Password should have at least 1 uppercase letter, 1 lowercase letter and 1 numeric value')
        return redirect('/')
    elif (request.form['password']) != (request.form['confirm_password']):
        flash('Password and Password Confirmation should match')
        return redirect('/')
    else:
        flash('Thanks for submitting your information.')
        return redirect('/')

app.run(debug=True) 