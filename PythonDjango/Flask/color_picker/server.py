from flask import Flask, render_template, request, redirect
app = Flask(__name__)

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/', methods=['post'])
def color():
	add_values = request.form['red'] + request.form['green'] + request.form['blue']
	return render_template("index.html", bgcolor = add_values)

app.run(debug=True)