from flask import Flask, render_template, session, request
app = Flask(__name__)
app.secret_key = 'rawr'

@app.route("/")
def index():
    if 'count' not in session:
        session['count'] = 0
    else:
        session['count'] = session['count']+1
    return render_template("index.html", count=session['count'])

@app.route("/add2", methods=["post"])
def add2():
    session['count'] = session['count']+2
    return render_template("index.html", count=session['count'])

@app.route("/reset", methods=["post"])
def reset():
    session['count'] = 1
    return render_template("index.html", count=session['count'])

app.run(debug=True)

