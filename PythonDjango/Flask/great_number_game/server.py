from flask import Flask, render_template, redirect, session, request
import random

app = Flask(__name__)
app.secret_key = 'rawr'

@app.route("/")
def index():
    if not 'number' in session:
        session['number'] = random.randint(0,100)
    if not 'string' in session:
        session['string'] = ""
    return render_template("index.html", number = session['number'], string = session['string'])

@app.route("/", methods=["post"])
def setting(): 
    if session['number'] > int(request.form["guess"]) :
        session['string'] = "Too low!"
    elif session['number'] < int(request.form["guess"]) :
        session['string'] = "Too high!"
    else:
        session['string'] = "yay you guessed correctly!"
    return redirect("/")

@app.route("/reset")
def reset(): 
    session.clear()
    return redirect("/")

app.run(debug=True)