from flask import Flask, render_template, redirect, session, request
from datetime import datetime
import random

app = Flask(__name__)
app.secret_key = 'rawr'

@app.route("/")
def index():
    if not 'gold' in session:
        session['gold'] = 0
    if not 'activity' in session:
        session['activity'] = []
    return render_template("index.html", gold = session['gold'], activity = session['activity'])

@app.route("/process_money", methods=["post"])
def setting(): 
    timestamp = datetime.now().strftime('%Y/%m/%d %-I:%M %p')
    if (request.form["building"] == "farm"):
        addno = random.randint(10,20)
        session['gold'] = session['gold'] + addno
        session['activity'].insert(0,"Earned {} golds from the farm. ({})".format(addno, timestamp))
    elif (request.form["building"] == "cave"):
        addno = random.randint(5,10)
        session['gold'] = session['gold'] + addno
        session['activity'].insert(0,"Earned {} golds from the cave. ({})".format(addno, timestamp))
    elif (request.form["building"] == "house"):
        addno = random.randint(2,5)
        session['gold'] = session['gold'] + addno
        session['activity'].insert(0,"Earned {} golds from the house. ({})".format(addno, timestamp))
    elif (request.form["building"] == "casino"):
        addno = random.randint(-50,50)
        session['gold'] = session['gold'] + addno
        building = "casino"
        if addno > 0:
            session['activity'].insert(0,"Entered a casino and won {} golds. ({})".format(addno, timestamp))
        else:
            session['activity'].insert(0,"Entered a casino and lost {} golds. Ouch... ({})".format(abs(addno), timestamp))
    return redirect("/")

@app.route("/reset")
def reset(): 
    session.clear()
    return redirect("/")

app.run(debug=True)