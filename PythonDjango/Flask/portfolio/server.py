from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/projects")
def portfolio():
    return render_template("projects.html")

@app.route("/about")
def aboutme():
    return render_template("aboutme.html")

app.run(debug=True)