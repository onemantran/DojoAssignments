from flask import Flask, render_template, request, redirect
app = Flask(__name__)

@app.route('/')
def index():
	return render_template("index.html")

@app.route("/ninja")
def ninja():
	img = "tmnt.png"
	return render_template("ninja.html", turtleimg=img)

@app.route("/ninja/<color>")
def color(color):
	if color == "blue":
		img = "leonardo.jpg"
	elif color == "red":
		img = "raphael.jpg"
	elif color == "orange":
		img = "michelangelo.jpg"
	elif color == "purple":
		img = "donatello.jpg"
	else:
		img = "notapril.jpg"
	return render_template("ninja.html", turtleimg=img)

app.run(debug=True)