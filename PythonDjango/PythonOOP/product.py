""" Assignment: Product
The owner of a store wants a program to track products. Create a product class to fill the following requirements.

Product Class:
Attributes:
• Price
• Item Name
• Weight
• Brand
• Status: default "for sale"

Methods:
• Sell: changes status to "sold"
• Add tax: takes tax as a decimal amount as a parameter and returns the price of the item including sales tax
• Return: takes reason for return as a parameter and changes status accordingly. 
    If the item is being returned because it is defective, change status to "defective" and change price to 0. 
    If it is being returned in the box, like new, mark it "for sale". 
    If the box has been, opened, set the status to "used" and apply a 20% discount.
• Display Info: show all product details.

Every method that doesn't have to return something should return self so methods can be chained. """

class Product(object):
    def __init__(self, price, name, weight, brand):
        self.price = price
        self.name = name
        self.weight = weight
        self.brand = brand
        self.status = "For Sale"
        
    def display_info(self):
        print "Price: {}".format(self.price)
        print "Name: {}".format(self.name)
        print "Weight: {}".format(self.weight)
        print "Brand: {}".format(self.brand)
        print "Status: {}".format(self.status)
        return self

    def sell(self):
        self.status = "Sold"
        return self

    def add_tax(self, amount):
        self.price = self.price + self.price * amount
        return self

    def return_item(self, reason):
        if reason == "Defective":
            self.status = "Defective"  
            self.price = 0
        elif reason == "Like New":
            self.status = "For Sale"
        elif reason == "Opened":
            self.status = "Used"
            self.price = self.price - self.price * 0.20
        return self
        
product1 = Product(10, "Pear", "2lbs", "Generic",)

product1.display_info().add_tax(.40).display_info().sell().display_info()
product1.return_item("Opened").display_info()


