""" Multiples
Part I - Write code that prints all the odd numbers from 1 to 1000. Use the for loop and don't use a list to do this exercise.
Part II - Create another program that prints all the multiples of 5 from 5 to 1,000,000. """

# Part I
for count in range (1, 1000, 2):
    print count
# Part II
for count in range (5, 10000001, 5):
    print count

""" Sum List
Create a program that prints the sum of all the values in the list: a = [1, 2, 5, 10, 255, 3] """

a = [1, 2, 5, 10, 255, 3]
sum = 0                           
for i in range (0, len(a)):
    sum += a[i]
print sum

""" Average List
Create a program that prints the average of the values in the list: a = [1, 2, 5, 10, 255, 3] """

a = [1, 2, 5, 10, 255, 3]
sum = 0                           
for i in range (0, len(a)):
    sum += a[i]
print sum / len(a)

